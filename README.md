# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone git@bitbucket.org:asistent/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/simicgasper/stroboskop/commits/b9e824f679f71f515bd97ec54346d274dc66bf5a

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/simicgasper/stroboskop/commits/00f8500d998dd579cb6e88006fbb8b85a9cd3919

Naloga 6.3.2:
https://bitbucket.org/simicgasper/stroboskop/commits/12e3154f222e3aa604ca6a46d5a841168150977d

Naloga 6.3.3:
https://bitbucket.org/simicgasper/stroboskop/commits/3904f1ef1ec942ed3ce71e088a32d8b5d8adfb20

Naloga 6.3.4:
https://bitbucket.org/simicgasper/stroboskop/commits/0f74a27ee8940a0c1af4dcde65808c24786226c0

Naloga 6.3.5:

```
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/simicgasper/stroboskop/commits/2ca29298c1ceeccabe270139ec0c1d1f1e282e60

Naloga 6.4.2:
https://bitbucket.org/simicgasper/stroboskop/commits/3a55e54789a7a404f6ff17ef88d07307bbe56c1e

Naloga 6.4.3:
https://bitbucket.org/simicgasper/stroboskop/commits/65e497fc180a2e04cef43abd24824d93e9fa9ce4

Naloga 6.4.4:
https://bitbucket.org/simicgasper/stroboskop/commits/eccdac1c853834fddab440705688995393e43857